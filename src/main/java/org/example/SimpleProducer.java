package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class SimpleProducer {

    public static void main(String[] args) {
        String topicName = "Hello-Kafka";
        Properties props = new Properties();

        props.put("bootstrap.servers", "node1:9092");
        props.put("acks", "all");
        // 发送失败尝试重发次数
        props.put("retries", 0);
        props.put("batch.size", 16384);
        // 数据在缓冲区中保留的时长,0表示立即发送，为了减少网络耗时，需要设置这个值
        // 太大可能容易导致缓冲区满，阻塞消费者，太小容易频繁请求服务端
        // 单位为毫秒 默认为0
        props.put("linger.ms",1);
        // 缓冲区大小
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<Object, Object> producer = new KafkaProducer<>(props);
        for (int i =0; i<10; i++){
            String key = Integer.toString(i);
            String value = Integer.toString(i);
            producer.send(new ProducerRecord<>(topicName, key, value));
        }
        System.out.println("Message sent successfully");
        producer.close();
    }

}
