package org.example.code1934;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class MyProducer {
    public static void main(String[] args) {
        // 1.创建生产者对象
        Properties props = new Properties();
        props.put("bootstrap.servers", "node1:9092");//如果是kafka集群，值需要写多台机器，例如:"node1:9092,node2:9092,node3:9092"，保证能消费到数据
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms",1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<Object, Object> producer = new KafkaProducer<>(props);
        // 2.使用send()方法生产数据到kafka
        for (int i = 0; i < 10; i++) {
            producer.send(new ProducerRecord<>(
                "test1934",Integer.toString(i),"hello"+i));
        }
        // 3.关闭生产者对象
        producer.close();
    }
}
