package org.example.code1912;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class MyProducer {
    public static void main(String[] args) {
        // 1. 新建一个生产者对象
        Properties props = new Properties();
        props.put("bootstrap.servers","node1:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms",1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<Object, Object> producer = new KafkaProducer<>(props);


        // 2. 使用send()方法发送数据
        for (int i = 0; i < 10; i++) {
            producer.send(new ProducerRecord<Object, Object>(
                "test1912",Integer.toString(i),"hello"+i));

        }

        // 3. 关闭生产者
        producer.close();
    }
}
