package org.example.code1912;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class MyConsumer {
    public static void main(String[] args) {
        // 1.创建一个消费者对象
        Properties props = new Properties();
        props.put("bootstrap.servers","node1:9092");
        props.put("group.id","test");
        props.put("enable.auto.commit","true");
        props.put("auto.commit.interval.ms","1000");
        props.put("session.timeout.ms","30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");//注意是StringDeserializer
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<Object, Object> consumer = new KafkaConsumer<>(props);

        // 2.消费者订阅kafka的主题
        consumer.subscribe(Arrays.asList("test1912"));

        // 3.使用poll方法消费数据
        while (true){
            ConsumerRecords<Object, Object> records = consumer.poll(Duration.ofSeconds(3));
            for (ConsumerRecord<Object, Object> record : records) {
                System.out.printf(
                        "offset=%d, key=%s, value=%s\n",
                        record.offset(),record.key(),record.value());
            }
        }
    }
}
