package org.example;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class SimpleConsumer {
    public static void main(String[] args) {
        String topicName = "Hello-Kafka";
        Properties props = new Properties();

        props.put("bootstrap.servers","node1:9092");//如果是kafka集群，为了能确保消费到数据，需要写多台机器地址，例如："node1:9092,node2:9092,node3:9092"
        props.put("group.id","test");
        // 开启自动提交offset到kafka
        props.put("enable.auto.commit","true");
        // 自动提交时间间隔
        // The frequency in milliseconds that the consumer offsets are auto-committed to Kafka
        // if enable.auto.commit is set to true.
        // 消费者offsets每隔多少毫秒自动提交到kafka
        props.put("auto.commit.interval.ms","1000");

        props.put("session.timeout.ms","30000");
        // 设置反序列化类
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");//注意不是StringSerializer
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<Object, Object> consumer = new KafkaConsumer<>(props);
        // 消费者订阅主题
        consumer.subscribe(Arrays.asList(topicName));
        System.out.println("Subscribed to topic：" + topicName);
        while (true){
//            ConsumerRecords<Object, Object> records = consumer.poll(1000);
            // 消费者间隔多久轮询获取数据
            ConsumerRecords<Object, Object> records = consumer.poll(Duration.ofSeconds(5));
            for (ConsumerRecord<Object, Object> record : records) {
                System.out.printf("offset = %d, key = %s, value = %s\n",
                        record.offset(), record.key(), record.value());
            }
        }
    }
}
