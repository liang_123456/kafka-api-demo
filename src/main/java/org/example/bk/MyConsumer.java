package org.example.bk;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class MyConsumer {
    public static void main(String[] args) {
        //1.创建消费者对象
        Properties prop = new Properties();
        prop.put("bootstrap.servers","node1:9092");
        prop.put("group.id","test");
        prop.put("auto.commit.interval.ms","1000");
        prop.put("session.timeout.ms","1000");
        prop.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");//注意不是StringSerializer
        prop.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<Object, Object> consumer = new KafkaConsumer<>(prop);
        //2.消费者订阅主题
        consumer.subscribe(Arrays.asList("topic"));// 将数组转为List集合

        //3.使用poll方法消费数据
        while (true){
//            ConsumerRecords<Object,Object> records = consumer.poll(Duration.ofSeconds(5));
            ConsumerRecords<Object, Object> records = consumer.poll(Duration.ofSeconds(5));

            for (ConsumerRecord<Object, Object> record : records) {
                System.out.printf("offset=%d, key=%s, value=%s\n",
                        record.offset(),record.key(),record.value());
            }
        }


    }
}
