package org.example.bk;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.Future;

public class MyProducer {
    public static void main(String[] args) {
        // 1.创建kafka生产者对象
        Properties prop = new Properties();
        prop.put("bootstrap.servers","node1:9092");
        prop.put("acks","all");
        prop.put("retries","0");
        // 16k一个批量
        prop.put("batch.size", 16384);
        prop.put("linger.ms",5);
        prop.put("buffer.memory", 33554432);

        prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        prop.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<Object, Object> producer = new KafkaProducer<>(prop);


        // 2.使用send方法生产数据
        for (int i = 0; i < 10; i++) {
            producer.send(new ProducerRecord<>("topic", Integer.toString(i), Integer.toString(i)));
        }

        // 3.关闭生产者
        producer.close();

    }
}
